import './App.css';
import { useEffect, useState } from 'react';
import { Route, Switch } from 'react-router-dom'
import ScreenOne from "./components/ScreenOne"
import axios from './axios';
import ScreenTwo from './components/ScreenTwo';
import ScreenThree from './components/ScreenThree';
import "./components/ScreenOne.css"

const App = (props) => {
  const [users, setUsers] = useState([])
  const [products, setProducts] = useState([])
  const [activeUser, setActiveUser] = useState()

  useEffect(() => {
    fetchUser().then(res => {
      const users = res.data.data
      setUsers(users)
      setActiveUser(users[0])
    }).catch(error => {
      console.log("Error at fetchUser");
      console.log(error.message);
    })

    fetchProduct().then(res => {
      const products = res.data.data
      setProducts(products)
    }).catch(error => {
      console.log("Error at fetchProduct");
    })

  }, [])

  const fetchUser = async () => {
    return axios.get('/user/get-user')
  }

  const fetchProduct = async () => {
    return axios.get('/user/get-product')
  }
  let screenOneTemplate = <div className="lds-ripple"><div></div><div></div></div>
  
  if(users.length && products.length && activeUser && Object.keys(activeUser).length) {
    screenOneTemplate = (
      <div className="App">
        <Switch>
            <Route path="/" exact render={() => <ScreenOne users={[...users]} products={[...products]} activeUser={{...activeUser}} setActiveUser={(user) => setActiveUser(user)} />} />
            {/* @Anh: Add Route for screen2 & screen3 here */}
            <Route path="/screen-two" exact render={() => <ScreenTwo users={[...users]} activeUser={{ ...activeUser }} setActiveUser={(user) => setActiveUser(user)} />} />
            <Route path="/screen-three" exact render={() => <ScreenThree users={[...users]} products={[...products]} activeUser={{ ...activeUser }} setActiveUser={(user) => setActiveUser(user)} />} />
        </Switch>
      </div>
    )
  }
  return (
    screenOneTemplate
  );
}

export default App;
