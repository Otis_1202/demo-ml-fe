import React from "react";
import "./Product.css";
import { Card, Button } from "react-bootstrap";

const Product = (props) => {
  const { name, image, price, _id } = props.product;
  const { handleInteractive, allowAction } = props

  return (
    <div className="Product">
      <Card>
        <Card.Img variant="top" src={image} className="ProductImg" />
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Text>{price} VNĐ</Card.Text>
          { allowAction && <div className="d-flex flex-row justify-content-between">
            <Button
              onClick={() => {
                handleInteractive(_id, "VIEW");
              }}
              variant="outline-primary"
            >
              View
            </Button>
            <Button
              onClick={() => {
                handleInteractive(_id, "LIKE");
              }}
              variant="outline-success"
            >
              Like
            </Button>
            <Button
              onClick={() => {
                handleInteractive(_id, "BUY");
              }}
              variant="outline-danger"
            >
              Buy
            </Button>
          </div> }
        </Card.Body>
      </Card>
    </div>
  );
};

export default Product;
