import { useEffect, useState } from "react"
import User from './UserComponent';
import Product from './ProductComponent';
import { Container, Row, Col, Table } from "react-bootstrap"
import { Link } from 'react-router-dom'
import axios from '../axios'
import "./User.css"
import "./ScreenOne.css"
const ScreenThree = (props) => {
    const { users, products, activeUser, setActiveUser } = props
    const [recommendedProducts, setRecommendedProducts] = useState([]);
    const [input, setInput] = useState(3);
    const [matrix, setMatrix] = useState([])
    const changeValue = (e) => {
        setInput(e.target.value);
    }
    const getRecommendedProducts = async (user, input) => {
        return axios.get('/user/find-top-product', {
            params: {
                k: input,
                user_id: user._id
            }
        });
    }
    const fetchMaxtrix = async () => {
        return axios.get("/user/get-matrix-full");
    };

    useEffect(() => {
        if(input > 0) {
            getRecommendedProducts(activeUser, input).then((res) => {
                setRecommendedProducts(res.data.data);
            })
        }
    }, [activeUser, input])

    useEffect(() => {
        fetchMaxtrix().then(res => {
            console.log(res.data.data);
            setMatrix(res.data.data);
        })
    }, [])
    
    return (
        <div className="ScreenOne mt-3">
            <Container fluid>
                <Row>
                    <Col xs={2}>
                        <Link to="/screen-two" className="btn btn-warning">
                            Back
                        </Link>
                    </Col>
                    <Col xs={10} className="text-danger font-weight-bold text-uppercase">
                        Recommended Product
                    </Col>
                    <Col xs={2}>
                        <form>
                            <label className="my-3 font-weight-bold text-danger">
                                The number of similar products:
                            </label>
                            <input className="form-control" type="number" value={input} onChange={(e) => changeValue(e)} />
                        </form>
                        {users.map(user => <User user={{ ...user, active: user._id === activeUser._id }} key={user._id} click={() => setActiveUser(user)}></User>)}
                    </Col>
                    <Col xs={10}>
                        <Row className="similar-user px-5">
                            {recommendedProducts.map(product => <Product allowAction={false} product={{ ...product._doc }}
                                key={product._id}></Product>)}
                        </Row>
                        <Row className="px-5 mt-3">
                            <Col xs={12}>
                                <Table responsive>
                                    <thead>
                                    <tr>
                                        <th className="font-weight-bold text-danger">User / Product</th>
                                        {products.map((product, index) => (
                                        <th key={index}>
                                            <img
                                            width="30"
                                            style={{ borderRadius: 50 }}
                                            height="30"
                                            src={product.image}
                                            alt={product._id}
                                            ></img>
                                            <br></br>
                                            {/* {product.name} */}
                                        </th>
                                        ))}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {users.map((user, index) => (
                                        <tr key={`u${index}`} className="text-center">
                                        <td className="font-weight-bold">
                                            <img
                                            width="30"
                                            style={{ borderRadius: 50 }}
                                            height="30"
                                            src={user.avatar}
                                            alt={user._id}
                                            ></img>{" "}
                                            {/* {user.first_name} {user.last_name} */}
                                        </td>
                                        {matrix.length &&
                                            matrix[index].map((item, index2) => (
                                            <td
                                                key={index2}
                                                className={ user._id === activeUser._id &&
                                                    recommendedProducts.findIndex(
                                                      (el) => el.index === index2
                                                    ) > -1 ? "oval-active" : !item.is_exists ? "text-danger" : ""}
                                            >
                                                {item.value.toFixed(3)}
                                            </td>
                                            ))}
                                        </tr>
                                    ))}
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </div >
    )
}
export default ScreenThree