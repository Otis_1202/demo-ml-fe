import React from 'react'
import "./User.css"

const User = (props) => {
    const { avatar, first_name, last_name, active } = props.user
    const style = {
        background: `url(${avatar})`
    }
    const avatarClass = active ? "Avatar border-danger" : "Avatar"
    const nameClass = active ? "Name text-danger font-weight-bold" : "Name"

    return (
        <div className="User" onClick={props.click}>
            <div className={avatarClass} style={style}></div>
            <p className={nameClass}>{`${first_name} ${last_name}`}</p>
        </div>
    )
}

export default User