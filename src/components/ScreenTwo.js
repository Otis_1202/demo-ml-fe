import { useEffect, useState } from "react"
import User from './UserComponent';
import { Container, Row, Col, Table } from "react-bootstrap"
import { Link } from 'react-router-dom'
import axios from '../axios'
import "./User.css"
const ScreenTwo = (props) => {
    const { users, activeUser, setActiveUser } = props
    const [userSimilars, setUserSimilars] = useState([]);
    const [input, setInput] = useState(3);
    const [matrix, setMatrix] = useState([])
    const changeValue = (e) => {
        setInput(e.target.value);
    }
    const getUserSimilar = async (user, input) => {
        return axios.get('/user/find-top-neighbor', {
            params: {
                k: input,
                user_id: user._id
            }
        });
    }

    const fetchMaxtrix = async () => {
        return axios.get("/user/get-matrix-similar");
    };

    useEffect(() => {
        if(input > 0) {
            getUserSimilar(activeUser, input).then(res => {
                setUserSimilars(res.data.data)
            })
        }
    }, [activeUser, input])

    useEffect(() => {
        fetchMaxtrix().then(res => {
            console.log(res.data.data);
            setMatrix(res.data.data);
        })
    }, [])

    return (
        <div className="ScreenOne mt-3">
            <Container fluid>
                <Row>
                    <Col xs={2}>
                        <Link to="/" className="btn btn-warning">
                            Back
                        </Link>
                        <Link to="/screen-three" className="btn btn-warning">Next</Link>
                    </Col>
                    <Col xs={10} className="text-danger font-weight-bold text-uppercase">
                        Similar User
                    </Col>
                    <Col xs={2}>
                        <form>
                            <label className="my-3 font-weight-bold text-danger">
                                The number of similar users:
                            </label>
                            <input className="form-control" type="number" value={input} onChange={(e) => changeValue(e)} />
                        </form>

                        {users.map(user => <User user={{ ...user, active: user._id === activeUser._id }} key={user._id} click={() => setActiveUser(user)}></User>)}
                    </Col>
                    <Col xs={10}>
                        <Row className="similar-user px-5">
                            {userSimilars.map(user => <User user={{ ...user }} key={user._id}></User>)}
                        </Row>
                        <Row className="px-5 mt-3">
                            <Col xs={12}>
                            {matrix && (
                            <Table responsive bordered>
                                <thead>
                                <tr>
                                    <th className="font-weight-bold text-danger">User / User</th>
                                    {users.map((user, index) => (
                                    <th key={index}>
                                        <img
                                        width="30"
                                        style={{ borderRadius: 50 }}
                                        height="30"
                                        src={user.avatar}
                                        alt={user._id}
                                        ></img>
                                        <br></br>
                                    </th>
                                    ))}
                                </tr>
                                </thead>
                                <tbody>
                                {users.map((user, index) => (
                                    <tr key={`u${index}`} className="text-center">
                                        <td className="font-weight-bold">
                                            <img
                                            width="30"
                                            style={{ borderRadius: 50 }}
                                            height="30"
                                            src={user.avatar}
                                            alt={user._id}
                                            ></img>{" "}
                                        </td>
                                        {matrix.length && matrix[index].map((value, index2) => (
                                        <td
                                            key={index2}
                                            className={ index === index2 ? "text-danger font-weight-bold" : ""}
                                        >
                                            {value ? value.toFixed(3) : value}
                                        </td>
                                        ))}
                                    </tr>
                                ))}
                                </tbody>
                            </Table>
                            )}
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </div >
    )
}
export default ScreenTwo