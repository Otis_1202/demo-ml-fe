import { useEffect, useState } from "react";
import { Container, Row, Col, Modal, Button, Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import User from "./UserComponent";
import Product from "./ProductComponent";
import axios from "../axios";
import "./ScreenOne.css";
const ScreenOne = (props) => {
  const { users, products, activeUser, setActiveUser } = props;

  const [show, setShow] = useState(false);
  const [matrix, setMatrix] = useState([]);

  useEffect(() => {
    if(show) {
        fetchMaxtrix()
        .then((res) => {
            setMatrix(res.data.data);
        })
        .catch((error) => {
            console.log("Error at fetchMatrix");
            console.log(error.message);
        });
    }
  }, [show]);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const fetchMaxtrix = async () => {
    return axios.get("/user/get-matrix");
  };

  const interactive = async (product_id, type_action) => {
    return axios.post("/user/interactive", {
      user_id: activeUser._id,
      product_id,
      type_action,
    });
  };
  
  const handleAction = async (product_id, type_action) => {
    interactive(product_id, type_action).then(() => {
        alert(`${type_action} PRODUCT SUCCESS`)
    })
  }

  return (
    <div className="ScreenOne mt-3">
      <Container fluid>
        <Row>
          <Col xs={2}>
            <Button variant="info" onClick={handleShow} className="mx-2">
              Show
            </Button>
            <Link to="/screen-two" className="btn btn-warning">
              Next
            </Link>

            {users.map((user) => (
              <User
                user={{ ...user, active: user._id === activeUser._id }}
                key={user._id}
                click={() => setActiveUser(user)}
              ></User>
            ))}
          </Col>
          <Col xs={10}>
            {products.map((product) => (
              <Product
                allowAction
                handleInteractive={handleAction}
                product={{ ...product }}
                key={product._id}
              ></Product>
            ))}
          </Col>
        </Row>
      </Container>

      {matrix && (
        <Modal show={show} size="xl" onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Interactive Matrix</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Table responsive>
                <thead>
                <tr>
                    <th className="font-weight-bold text-danger">User / Product</th>
                    {products.map((product, index) => (
                    <th key={index}>
                        <img
                        width="30"
                        style={{ borderRadius: 50 }}
                        height="30"
                        src={product.image}
                        alt={product._id}
                        ></img>
                        <br></br>
                        {/* {product.name} */}
                    </th>
                    ))}
                </tr>
                </thead>
                <tbody>
                {users.map((user, index) => (
                    <tr key={`u${index}`} className="text-center">
                    <td className="font-weight-bold">
                        <img
                        width="30"
                        style={{ borderRadius: 50 }}
                        height="30"
                        src={user.avatar}
                        alt={user._id}
                        ></img>{" "}
                        {/* {user.first_name} {user.last_name} */}
                    </td>
                    {matrix.length &&
                        matrix[index].map((value, index) => (
                        <td
                            key={index}
                            className={value === "o" ? "text-danger" : ""}
                        >
                            {value}
                        </td>
                        ))}
                    </tr>
                ))}
                </tbody>
            </Table>
          </Modal.Body>
        </Modal>
      )}
    </div>
  );
};

export default ScreenOne;
