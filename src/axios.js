import axios from "axios";

const instance = axios.create({
  baseURL: "http://localhost:7004/api/v1/",
});

export default instance;
